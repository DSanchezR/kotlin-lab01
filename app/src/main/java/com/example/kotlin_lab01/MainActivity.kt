package com.example.kotlin_lab01

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnProcesar.setOnClickListener {
            val edad: Int = edtEdad.text.toString().toInt()
            tvResultado.text = when {
                edad >= 18 -> "Usted es mayor de edad"
                else -> "Usted es menor de edad"
            }
        }
    }
}